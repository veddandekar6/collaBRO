<?php
require_once "database_calls/db.php";
$db = new DB();
session_start();
if (!isset($_SESSION["userid"]))
    header("location:index.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>collaBRO</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="manifest" href="assets/img/site.webmanifest">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Patrick+Hand&display=swap" rel="stylesheet">
    <script src="assets/js/main.js"></script>
</head>

<body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="https://gitlab.com/veddandekar6/collabBRO">colla<span
                        class="green">BRO</span></a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Boards</a></li>
                <li><a href="archives.php">Archives</a></li>
                <li>
                    <input type="text" id="searchInput" class="form-control" placeholder="Search board or list names">
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a data-toggle="modal" data-target="#addModal"><span class="glyphicon glyphicon-plus"></span>
                        Add</a></li>
                <li><a href="database_calls/end_session.php"><span class="glyphicon glyphicon-log-out"></span>
                        Logout</a></li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <div id="main_space" class="row-sm-12">
            <?php
            $query = "SELECT board_id FROM board_users WHERE user_id=" . $_SESSION["userid"] . " AND archive=0";
            $boards = $db->run_query($query);
            if (mysqli_num_rows($boards) != 0) {
                while ($row = mysqli_fetch_assoc($boards)) {
                    $query = "SELECT name, shared, deadline FROM boards WHERE board_id=" . $row["board_id"];
                    $name = $db->run_query($query);
                    $name = mysqli_fetch_row($name);
                    $boardArr[$row["board_id"]] = $name[0];
                    if ($name[0]) {
                        echo '<div class="card col-xs-11 col-sm-5 col-lg-3" id="board' . $row["board_id"] . '">
              <div class="main">' . $name[0];
                        if ($name[1] == "1")
                            echo '<div class="middle"><span class="glyphicon glyphicon-globe"></span></div>';
                        if ($name[2]) {
                            echo '<div class="top">' . $name[2];
                            $start = strtotime($name[2]);
                            $end = strtotime(date('Y-m-d'));
                            $days = ceil(abs($end - $start) / 86400);
                            if ($days < 2)
                                echo '<br /><div class="deadlineFont">DEADLINE!</div>';
                            echo '</div>';
                        }
                        echo '</div>
              <div id="B' . $row["board_id"] . '" class="behind">';
                        $query = "SELECT list_name, list_id FROM list WHERE board_id=" . $row["board_id"] . " LIMIT 5";
                        $lists = $db->run_query($query);
                        if (mysqli_num_rows($lists) != 0) {
                            echo '<ul>';
                            while ($list = mysqli_fetch_row($lists)) {
                                echo '<li data-toggle="modal" data-target="#listModal" data-id="' . $list[1] . '">' . $list[0] . '</li>';
                            }
                            echo '</ul>';
                        }
                        echo '<div class="bottom">
                  <b>
                    <div class="left" data-toggle="modal" data-target="#deleteBoardModal" data-id="' . $row["board_id"] . '">Remove</div>
                    <div class="middle" data-toggle="modal" data-target="#shareBoardModal" data-id="' . $row["board_id"] . '">Share</div>
                    <div class="right" data-toggle="modal" data-target="#addListModal" data-id="' . $row["board_id"] . '">New</div>
                  </b>
                </div>
              </div>
            </div>';
                    }
                }
            }
            ?>

            <!-- MODALS HERE -->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalTitle">Add new board</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-check">
                                <label for="addModalName">Enter name</label>
                                <input type="text" class="form-control" id="addModalName" placeholder="Enter name">
                            </div>
                            <br />
                            <div class="form-check">
                                <label for="deadline">Deadline</label>
                                <input type="date" class="form-control" id="deadline">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" onClick="addNewBoard()" class="btn btn-primary"
                                data-dismiss="modal">Create</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="deleteBoardModal" tabindex="-1" role="dialog" aria-labelledby="deleteBoardModal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Do you wish to archive or delete?
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="archiveBoardButton" class="btn btn-secondary"
                                data-dismiss="modal">Archive</button>
                            <button type="button" id="deleteBoardButton" class="btn btn-primary"
                                data-dismiss="modal">Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="shareBoardModal" tabindex="-1" role="dialog" aria-labelledby="shareBoardModal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="shareModalTitle">Share Board</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-check">
                                <label for="addModalName">Enter username</label>
                                <input type="text" class="form-control" id="shareModalUsername"
                                    placeholder="Enter username">
                            </div>
                            <div>
                                <label>Currently available to </label>
                                <ul id="membersList">
                                </ul>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" id="shareBoardButton" class="btn btn-primary">Share</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="addListModal" tabindex="-1" role="dialog" aria-labelledby="addListModal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addListModalTitle">Add new list</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-check">
                                <label for="addModalName">Enter name</label>
                                <input type="text" class="form-control" id="addListModalName" placeholder="Enter name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" id="addListButton" class="btn btn-primary"
                                data-dismiss="modal">Create</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="listModal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="listModalTitle"></h5>
                        </div>
                        <div class="modal-body">
                            <h3>Checklist</h3>
                            <div id="checkboxesElement">
                            </div>
                            <form id="fileUploadForm" enctype="multipart/form-data">
                                <h3>Attachments</h3>
                                <div id='uploadedFiles'>
                                </div> <br />
                                <input type="file" class="form-control-file" name="fileToUpload" id="fileToUpload">
                                <br />
                            </form>
                            <h3>Notes</h3>
                            <ul id='uploadedNotes' class="list-group list-group-flush">
                            </ul>
                            <input type="textarea" class="form-control" id="newNoteField"
                                placeholder="Write a note"><br />
                            <button id="addNewNote" class='btn btn-success' onClick="addNewNote()">Add
                                New</button><br />
                        </div>

                        <div class="modal-footer">
                            <div class="dropup">
                                <button class="btn btn-primary dropdown-toggle" type="button"
                                    data-toggle="dropdown">Move
                                    <span class="caret"></span></button>
                                <ul id="boardChanges" class="dropdown-menu">
                                    <?php
                                    if (isset($boardArr)) {
                                        foreach ($boardArr as $id => $name) {
                                            if ($name == "")
                                                continue;
                                            echo '<li id=' . $id . ' onClick="moveList(this.id)"><a>' . $name . '</a></li>';
                                        }
                                    }
                                    ?>
                                </ul>
                                <button class="btn btn-danger dropdown-toggle" type="button" id="deleteListBtn"
                                    data-dismiss="modal">Delete
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>