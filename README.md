## **collaBRO**

collaBRO is an online web-based collaboration tool developed with
simplicity and productivity in mind. This project targets to increase
user efficiency by providing a simple to use package that boasts all
necessary features. collaBRO was developed as a submission to 3 day
Hackathon where the PS was to create a tool similar to Trello.

**Gitlab -** https://gitlab.com/veddandekar6/collaBRO
**Live Demo -** http://15.206.185.10/

## **Features**

- Organize tasks into **collections**.

- Simplify tasks by using **check lists, quick notes or by using file
  attachments**.

- Invite users to instantly create **shared collections** for
  collaboration.

- **Keep track** of which user made what change.

- **Search** through boards and lists quickly.

- **Archive collections** to keep what is important front and
  foremost.

- Support quick and **simple** **moving of items** between
  collections.

- **Deadline alerts** at a glance.

- Simple fluid UI that works across **desktop and mobile**.

## **Features explained**

- In order of hierarchy =\>

  - The highest order is of Boards.

  - Each board is further divided in lists.

  - Each list is a collection of checklists, attachments and notes.

- Each board can be shared with other users.

  - Shared boards have a globe symbol below their name.

  - Every user who has access to the board can see and add to any
    list of the board.

  - collaBRO logs each time a user adds or makes a change to an
    item inside a list. To see which user made the change, simply
    hover over the item to see username in a tooltip. (See
    screenshot for better understanding)

  - **Note**: If a user deletes a board, he simply removes his
    access to it. A board is only removed from database when all
    users delete it. List items, on the other hand, are removed from
    everyone if a single user removes it.

- A separate page named "Archives" is maintained to provide a way for
  each user to declutter his/her view.

  - **Note:** For shared collections, if one user archives a board,
    it will be archived only from his view. Every other user can
    access the board as usual.

- Deadline alerts are provided in the form of the word "DEADLINE"
  written in red over the board when not hovered. This alert starts
  displaying 2 days before the provided deadline is reached.

## **Screenshots**

![](media/ss1.jpg)
![](media/ss1_2.jpg)
![](media/ss2.jpg)
