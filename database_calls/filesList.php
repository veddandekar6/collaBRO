<?php
require_once "db.php";
$db = new DB();
session_start();
if ($_POST["id"] && isset($_SESSION["userid"])) {

	$_POST["id"] = htmlentities($_POST["id"]);
	$_SESSION["userid"] = htmlentities($_SESSION["userid"]);

	$query = "SELECT location, doneby FROM files WHERE list_id=" . $_POST["id"];
	$result = $db->run_query("$query");
	if (mysqli_num_rows($result) > 0) {
		$rows = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
}