<?php
require_once "db.php";
$db = new DB();
session_start();
if ($_POST["id"] && $_POST["key"] && isset($_SESSION["userid"])) {

    $_POST["id"] = htmlentities($_POST["id"]);
    $_POST["key"] = htmlentities($_POST["key"]);
    $_SESSION["userid"] = htmlentities($_SESSION["userid"]);

    $query = "DELETE FROM board_users WHERE board_id=" . $_POST["id"] . " AND user_id=" . $_POST["key"];
    $db->run_query($query);
    $query = "SELECT * FROM board_users WHERE board_id=" . $_POST["id"];
    $result = $db->run_query($query);
    if (mysqli_num_rows($result) == 1) {
        $query = "UPDATE boards SET shared=0 WHERE board_id=" . $_POST['id'];
        $db->run_query($query);
    }
    $query = "SELECT board_id FROM boards WHERE board_id NOT IN (SELECT board_id FROM board_users)";
    $result = $db->run_query($query);
    if (mysqli_num_rows($result) != 0) {
        while ($row = mysqli_fetch_row($result)) {
            $query = "DELETE FROM boards WHERE board_id=" . $row[0];
            $db->run_query($query);
        }
    }
}