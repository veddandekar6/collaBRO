<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

if (isset($_POST["submit"])) {
    $uploadOK = 1;
}

if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}

if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}

if (
    $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" && $imageFileType != "pdf"
) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}

if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $finalname = explode(".", $target_file)[0] . time() . "." . explode(".", $target_file)[1])) {
        require_once "db.php";
        $db = new DB();
        session_start();
        if ($_POST["list_id"] && isset($_SESSION["userid"])) {
            $query = "SELECT username FROM users WHERE user_id=" . $_SESSION["userid"];
            $result = $db->run_query("$query");
            $result = mysqli_fetch_row($result);
            $query = "INSERT INTO files VALUES(NULL, " . $_POST["list_id"] . ", '" . $finalname . "', '" . $result[0] . "')";
            $db->run_query("$query");
        }
        echo "success " . $finalname;
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}