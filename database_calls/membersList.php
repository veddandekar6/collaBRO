<?php
require_once "db.php";
$db = new DB();
session_start();
if ($_POST["id"] && isset($_SESSION["userid"])) {

    $_POST["id"] = htmlentities($_POST["id"]);
    $_SESSION["userid"] = htmlentities($_SESSION["userid"]);

    $query = "SELECT user_id FROM board_users WHERE board_id=" . $_POST["id"];
    $result = $db->run_query("$query");
    $rows = array();
    while ($r = mysqli_fetch_row($result)) {
        $query = "SELECT username FROM users WHERE user_id=" . $r[0];
        $result1 = $db->run_query("$query");
        $result1 = mysqli_fetch_row($result1);
        $rows[$r[0]] = $result1[0];
    }
    print json_encode($rows);
}