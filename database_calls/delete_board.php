<?php
require_once "db.php";
$db = new DB();
session_start();
if ($_POST["id"] && isset($_SESSION["userid"])) {

    $_POST["id"] = htmlentities($_POST["id"]);
    $_SESSION["userid"] = htmlentities($_SESSION["userid"]);

    $query = "DELETE FROM board_users WHERE board_id=" . $_POST["id"] . " AND user_id=" . $_SESSION["userid"];
    if ($db->run_query("$query")) {
        $query = "SELECT board_id FROM boards WHERE board_id NOT IN (SELECT board_id FROM board_users)";
        $result = $db->run_query($query);
        if (mysqli_num_rows($result) != 0) {
            while ($row = mysqli_fetch_row($result)) {
                $query = "DELETE FROM boards WHERE board_id=" . $row[0];
                if ($db->run_query($query))
                    echo "success";
            }
        }
    }
}