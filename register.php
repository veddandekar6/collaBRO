<?php
require_once 'database_calls/db.php';
$db = new DB();
if (isset($_POST['register'])) {
    $username = htmlentities($_POST["username"]);
    $password = htmlentities($_POST["password"]);
    $email = htmlentities($_POST["email"]);
    if ($username != '' and $password != '' and $email != '') {
        if ($password != $_POST["cpassword"])
            $err = 2;
        else {
            $query = "SELECT * FROM users where username = '$username'";
            $result = $db->run_query($query);
            if (mysqli_num_rows($result))
                $err = 3;
            $query = "SELECT * FROM users where email = '$email'";
            $result = $db->run_query($query);
            if (mysqli_num_rows($result))
                $err = 4;
            if (!isset($err)) {
                $query = "INSERT INTO users VALUES(NULL, '$email','$username', SHA1('$password'));";
                $result = $db->run_query($query);
                header("location:index.php?success=1");
            }
        }
    } else
        $err = 1;
}
?>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>collabBRO - Register</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="manifest" href="assets/img/site.webmanifest">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/index.css">
</head>

<body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <section class="container-fluid">
        <section class="row justify-content-center">
            <section class="col-12 col-sm-6 col-md-4">
                <?php
                if (isset($err)) {
                    if ($err == 1)
                        echo '<div class="alert alert-danger"><strong>Please fill all the fields</strong></div>';
                    else if ($err == 2)
                        echo '<div class="alert alert-danger"><strong>Passwords do not match</strong></div>';
                    else if ($err == 3)
                        echo '<div class="alert alert-danger"><strong>Username already exists</strong></div>';
                    else if ($err == 4)
                        echo '<div class="alert alert-danger"><strong>Email already registered</strong></div>';
                }
                ?>

                <form class="form-container" method="POST">
                    <div class="form-group">
                        <h4 class="text-center font-weight-bold"> Login </h4>
                        <label for="InputEmail1">Username</label>
                        <input type="text" class="form-control" id="username" name="username"
                            placeholder="Enter username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                            placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="cpassword">Verify password</label>
                        <input type="password" class="form-control" id="cpassword" name="cpassword"
                            placeholder="Verify Password">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" name="register"
                        value="reg">Register</button>

                    </div>
                </form>
            </section>
        </section>
    </section>
</body>

</html>