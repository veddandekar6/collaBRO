<?php
require_once 'database_calls/db.php';
if (isset($_POST["submit"])) {
    $username = trim($_POST["username"]);
    $password = trim($_POST["password"]);
    if ($username == "" or $password == "") {
        $err = 1;
    } else {
        $db = new DB();
        $query = "SELECT * FROM users WHERE username='$username' AND password=SHA1('$password');";
        $result = $db->run_query($query);
        if (mysqli_num_rows($result)) {
            $query = "SELECT user_id FROM users WHERE username='$username'";
            $result = $db->run_query($query);
            $result = mysqli_fetch_row($result);
            session_start();
            $_SESSION["userid"] = $result[0];
            header("location:personal.php");
        } else
            $err = 1;
    }
}
?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>collaBRO - Login</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="manifest" href="assets/img/site.webmanifest">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/index.css">
</head>

<body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <section class="container-fluid">
        <section class="row justify-content-center">
            <section class="col-12 col-sm-6 col-md-4">
                <?php
                if (isset($err))
                    echo '<div class="alert alert-danger"><strong>Invalid username/password</strong></div>';
                if (isset($_GET["success"]))
                    echo '<div class="alert alert-success"><strong>User Registered Successfully</strong></div>';
                ?>
                <form class="form-container" method="POST">
                    <div class="form-group">
                        <h4 class="text-center font-weight-bold"> Login </h4>
                        <label for="InputEmail1">Username</label>
                        <input type="text" class="form-control" id="username" name="username"
                            placeholder="Enter username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                            placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" name="submit">Login</button>
                    <div class="form-footer">
                        <p> Don't have an account? <a href="register.php">Sign Up</a></p>
                    </div>
                </form>
            </section>
        </section>
    </section>
</body>

</html>