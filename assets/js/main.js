function addNewBoard() {
  var name = document.getElementById("addModalName").value;
  document.getElementById("addModalName").value = "";
  var deadline = document.getElementById("deadline").value;
  window.id = -1;

  $.ajax({
    data: "name=" + name + "&deadline=" + deadline,
    async: false,
    url: "database_calls/add_new_board.php",
    method: "POST",
    success: function(ret) {
      if (ret) window.id = parseInt(ret);
    }
  });

  if (window.id == -1) alert("Database error");
  else {
    var node = document.createElement("div");
    node.classList.add("card", "col-xs-11", "col-sm-5", "col-lg-3");
    node.id = "board" + window.id;
    var node2 = document.createElement("div");
    node2.classList.add("main");
    node2.innerText = name;
    var node3 = document.createElement("div");
    node3.classList.add("top");
    node3.innerText = deadline;
    node2.append(node3);
    node.append(node2);
    node2 = document.createElement("div");
    node2.classList.add("behind");
    node2.id = "B" + window.id;
    node3 = document.createElement("ul");
    node2.append(node3);
    node3 = document.createElement("div");
    node3.classList.add("left");
    node3.setAttribute("data-toggle", "modal");
    node3.setAttribute("data-target", "#deleteBoardModal");
    node3.setAttribute("data-id", window.id);
    node3.innerText = "Remove";
    node2.append(node3);
    node3 = node3.cloneNode(true);
    node3.classList.remove("left");
    node3.classList.add("middle");
    node3.setAttribute("data-target", "#shareBoardModal");
    node3.innerText = "Share";
    node2.append(node3);
    node3 = node3.cloneNode(true);
    node3.classList.remove("middle");
    node3.classList.add("right");
    node3.setAttribute("data-target", "#addListModal");
    node3.innerText = "Add";
    node2.append(node3);
    node.append(node2);
    document.getElementById("main_space").append(node);
    var boardChanges = document.getElementById("boardChanges");
    node = document.createElement("li");
    node.id = window.id;
    node.addEventListener("click", function() {
      moveList(this.id);
    });
    node.append((document.createElement("a").innerText = name));
    boardChanges.append(node);
  }
}

function deleteBoard(id) {
  $.post("database_calls/delete_board.php", { id: id });
  document.getElementById("board" + id).remove();
}

function archiveBoard(id) {
  $.post("database_calls/archive_board.php", { id: id });
  document.getElementById("board" + id).remove();
}

function unarchiveBoard(id) {
  $.post("database_calls/unarchive_board.php", { id: id });
  document.getElementById("board" + id).remove();
}

function addNewList(id) {
  var name = document.getElementById("addListModalName").value;
  document.getElementById("addListModalName").value = "";
  window.id = -1;
  $.ajax({
    data: "id=" + id + "&name=" + name,
    async: false,
    url: "database_calls/add_list.php",
    method: "POST",
    success: function(ret) {
      if (ret) window.id = parseInt(ret);
    }
  });
  if (window.id == -1) alert("Database error");
  else {
    var node = document.createElement("li");
    node.setAttribute("data-toggle", "modal");
    node.setAttribute("data-target", "#listModal");
    node.setAttribute("data-id", window.id);
    node.innerText = name;
    document.getElementById("B" + id).childNodes[0].append(node);
  }
}

function checkboxChange(e, id) {
  $.post("database_calls/checkboxChange.php", { id: id });
  if (e.checked) {
    e.nextElementSibling.style.textDecoration = "line-through";
    e.nextElementSibling.setAttribute("title", "Completed by you");
  } else {
    e.nextElementSibling.style.textDecoration = "";
    e.nextElementSibling.setAttribute("title", "Added by you");
  }
}

function deleteCheckBox(e, id) {
  $.post("database_calls/deleteCheckbox.php", { id: id });
  e.parentElement.remove();
}

function addNewChecklistItem(id) {
  var details = document.getElementById("newAddItem").value;
  document.getElementById("newAddItem").value = "";
  window.id = -1;
  $.ajax({
    data: "id=" + id + "&details=" + details,
    async: false,
    url: "database_calls/addNewChecklistItem.php",
    method: "POST",
    success: function(ret) {
      if (ret) window.id = parseInt(ret);
    }
  });
  if (window.id == -1) alert("Database error");
  else {
    var node = document.getElementById("newAddItem");
    node.remove();
    node = document.getElementById("newAddItem2");
    node.remove();
    var checkboxesElement = document.getElementById("checkboxesElement");
    var div = document.createElement("div");
    div.classList.add("form-check");
    node = document.createElement("input");
    node.type = "checkbox";
    node.id = window.id;
    node.addEventListener("click", function() {
      checkboxChange(this, window.id);
    });
    node.classList.add("form-check-input");
    div.append(node);
    node = document.createElement("label");
    node.htmlFor = window.id;
    node.innerText = details;
    node.classList.add("form-check-label");
    node.setAttribute("data-toggle", "tooltip");
    node.setAttribute("title", "Added by you");
    div.append(node);
    node = document.createElement("span");
    node.classList.add("glyphicon", "glyphicon-remove");
    node.addEventListener("click", function() {
      deleteCheckBox(this, window.id);
    });
    node.style.cursor = "pointer";
    div.append(node);
    checkboxesElement.append(div);
    node = document.createElement("p");
    node.innerText = "+ Add another";
    node.style.marginTop = "10px";
    node.style.marginBottom = "10px";
    node.addEventListener("click", function() {
      createNewChecklistItem(this, id);
    });
    node.style.cursor = "pointer";
    checkboxesElement.append(node);
  }
}

function createNewChecklistItem(e, id) {
  $(e).remove();
  var checkboxesElement = document.getElementById("checkboxesElement");
  var node = document.createElement("input");
  node.id = "newAddItem";
  checkboxesElement.append(node);
  node = document.createElement("button");
  node.id = "newAddItem2";
  node.innerText = "Add";
  node.classList.add("btn", "btn-success");
  node.addEventListener("click", function() {
    addNewChecklistItem(id);
  });
  checkboxesElement.append(node);
}

function shareBoard(id) {
  window.id = null;
  var username = document.getElementById("shareModalUsername").value;
  document.getElementById("shareModalUsername").value = "";
  $.ajax({
    data: "id=" + id + "&username=" + username,
    async: false,
    url: "database_calls/shareBoard.php",
    method: "POST",
    success: function(ret) {
      window.id = parseInt(ret);
    }
  });
  if (window.id) {
    var mem = document.createElement("li");
    mem.innerText = username;
    var node2 = document.createElement("span");
    node2.classList.add("glyphicon", "glyphicon-remove");
    node2.addEventListener("click", function() {
      removeUser(this, id, window.id);
    });
    mem.append(node2);
    document.getElementById("membersList").appendChild(mem);
  } else {
    alert("Username does not exist.");
  }
}

function removeUser(e, key, id) {
  $.post("database_calls/removeUser.php", { id: id, key: key });
  e.parentElement.remove();
}

function addNewNote() {
  var note = document.getElementById("newNoteField").value;
  document.getElementById("newNoteField").value = "";
  window.id = -1;
  $.ajax({
    data: "id=" + window.listID + "&note=" + note,
    async: false,
    url: "database_calls/addNewNote.php",
    method: "POST",
    success: function(ret) {
      if (ret) window.id = parseInt(ret);
    }
  });
  if (window.id == -1) alert("Database error");
  else {
    var notes = document.getElementById("uploadedNotes");
    var node = document.createElement("li");
    node.innerText = note;
    node.setAttribute("data-toggle", "tooltip");
    node.setAttribute("title", "Added by you");
    node.classList.add("list-group-item");
    var node2 = document.createElement("span");
    node2.classList.add("glyphicon", "glyphicon-remove");
    node2.addEventListener("click", function() {
      deleteNote(this, window.id);
    });
    node2.style.cursor = "pointer";
    node2.style.position = "absolute";
    node2.style.right = "10px";
    node2.style.paddingTop = "10px";
    node.append(node2);
    notes.append(node);
  }
}

function deleteList(id) {
  $.ajax({
    data: "id=" + id,
    async: false,
    url: "database_calls/deleteList.php",
    method: "POST",
    success: function() {
      $("li[data-id='" + id + "']").remove();
    }
  });
}

function moveList(id) {
  $.ajax({
    data: "id=" + window.listID + "&boardID=" + id,
    async: false,
    url: "database_calls/moveList.php",
    method: "POST",
    success: function(ret) {
      window.location.reload(true);
    }
  });
}

function deleteFile(e, fname) {
  $.post("database_calls/deleteFile.php", { name: fname });
  e.previousSibling.remove();
  e.nextSibling.remove();
  e.remove();
}

function deleteNote(e, id) {
  $.post("database_calls/deleteNote.php", { id: id });
  e.parentElement.remove();
}

$(document).ready(function() {
  $("#searchInput").on("keyup", function() {
    var value = $(this)
      .val()
      .toLowerCase();
    $(".card").filter(function() {
      $(this).toggle(
        $(this)
          .text()
          .toLowerCase()
          .indexOf(value) > -1
      );
    });
  });
  window.id = -1;
  $('[data-toggle="tooltip"]').tooltip();
  $("#deleteBoardModal").on("show.bs.modal", function(event) {
    var button = $(event.relatedTarget);
    var id = button.data("id");
    if (window.deleteBoardAnon)
      document
        .getElementById("deleteBoardButton")
        .removeEventListener("click", window.deleteBoardAnon);
    window.deleteBoardAnon = function() {
      deleteBoard(id);
    };
    document
      .getElementById("deleteBoardButton")
      .addEventListener("click", window.deleteBoardAnon);

    var archiveBoardBtn = document.getElementById("archiveBoardButton");
    if (archiveBoardBtn) {
      if (window.archiveBoardAnon)
        archiveBoardBtn.removeEventListener("click", window.archiveBoardAnon);
      window.archiveBoardAnon = function() {
        archiveBoard(id);
      };
      archiveBoardBtn.addEventListener("click", window.archiveBoardAnon);
    }
    var unarchiveBoardBtn = document.getElementById("unarchiveBoardButton");
    if (unarchiveBoardBtn) {
      if (window.unarchiveBoardAnon)
        unarchiveBoardBtn.removeEventListener(
          "click",
          window.unarchiveBoardAnon
        );
      window.unarchiveBoardAnon = function() {
        unarchiveBoard(id);
      };
      unarchiveBoardBtn.addEventListener("click", window.unarchiveBoardAnon);
    }
  });

  $("#addListModal").on("show.bs.modal", function(event) {
    var button = $(event.relatedTarget);
    var id = button.data("id");
    if (window.addNewListAnon)
      document
        .getElementById("addListButton")
        .removeEventListener("click", window.addNewListAnon);
    window.addNewListAnon = function() {
      addNewList(id);
    };
    document
      .getElementById("addListButton")
      .addEventListener("click", window.addNewListAnon);
  });

  $("#shareBoardModal").on("show.bs.modal", function(event) {
    var button = $(event.relatedTarget);
    var id = button.data("id");
    if (window.shareBoardAnon)
      document
        .getElementById("shareBoardButton")
        .removeEventListener("click", window.shareBoardAnon);
    window.shareBoardAnon = function() {
      shareBoard(id);
    };
    document
      .getElementById("shareBoardButton")
      .addEventListener("click", window.shareBoardAnon);
    $.ajax({
      data: "id=" + id,
      url: "database_calls/membersList.php",
      async: false,
      method: "POST",
      success: function(msg) {
        window.id = JSON.parse(msg);
      }
    });
    var mem = document.getElementById("membersList");
    mem.innerHTML = "";
    if (window.id) {
      var node = "";
      var wid = window.id;
      for (var key in wid) {
        node = document.createElement("li");
        node.innerText = wid[key];
        var node2 = document.createElement("span");
        node2.classList.add("glyphicon", "glyphicon-remove");
        node2.addEventListener("click", function() {
          removeUser(this, key, id);
        });
        node.append(node2);
        mem.append(node);
      }
    }
  });

  $("#listModal").on("show.bs.modal", function(event) {
    var element = $(event.relatedTarget);
    var id = element.data("id");
    window.listID = id;
    document.getElementById("listModalTitle").innerText = element[0].innerText;
    var checkboxesElement = document.getElementById("checkboxesElement");
    checkboxesElement.innerHTML = "";
    document.getElementById("uploadedNotes").innerHTML = "";
    $.ajax({
      data: "id=" + id,
      url: "database_calls/ret_list_checklists.php",
      method: "POST",
      success: function(msg) {
        msg = JSON.parse(msg);
        msg.forEach(function(row) {
          var div = document.createElement("div");
          div.classList.add("form-check");
          var node = document.createElement("input");
          node.type = "checkbox";
          node.id = row["checklist_id"];
          node.classList.add("form-check-input");
          node.addEventListener("click", function() {
            checkboxChange(this, row["checklist_id"]);
          });
          if (row["completed"] == "1") {
            var node1 = node;
            node.checked = "true";
          }
          div.append(node);
          node = document.createElement("label");
          node.htmlFor = row["checklist_id"];
          node.innerText = row["details"];
          node.classList.add("form-check-label");
          node.setAttribute("data-toggle", "tooltip");
          if (row["completed"] == "1")
            node.setAttribute("title", "Completed by " + row["doneby"]);
          else node.setAttribute("title", "Added by " + row["doneby"]);
          div.append(node);
          node = document.createElement("span");
          node.classList.add("glyphicon", "glyphicon-remove");
          node.addEventListener("click", function() {
            deleteCheckBox(this, row["checklist_id"]);
          });
          node.style.cursor = "pointer";
          div.append(node);
          checkboxesElement.append(div);
          if (row["completed"] == "1")
            node1.nextElementSibling.style.textDecoration = "line-through";
        });
        var node = document.createElement("p");
        node.innerText = "+ Add another";
        node.style.marginTop = "10px";
        node.addEventListener("click", function() {
          createNewChecklistItem(this, id);
        });
        node.style.cursor = "pointer";
        checkboxesElement.append(node);
      }
    });
    var filesListElement = document.getElementById("uploadedFiles");
    filesListElement.innerHTML = "";
    $.ajax({
      data: "id=" + id,
      url: "database_calls/filesList.php",
      async: false,
      method: "POST",
      success: function(msg) {
        if (!msg) return;
        var data = JSON.parse(msg);
        data.forEach(function(ele) {
          var node = document.createElement("p");
          node.innerText = ele["location"].split("/")[1];
          node.addEventListener("click", function() {
            window.open("database_calls/" + ele["location"]);
          });
          // node.style.float = "left";
          node.style.position = "absolute";
          node.style.left = "15px";
          node.style.cursor = "pointer";
          node.setAttribute("data-toggle", "tooltip");
          node.setAttribute("title", "Added by " + ele["doneby"]);
          filesListElement.append(node);
          node = document.createElement("span");
          node.classList.add("glyphicon", "glyphicon-remove");
          node.addEventListener("click", function() {
            deleteFile(this, ele["location"]);
          });
          node.style.cursor = "pointer";
          node.style.float = "right";
          filesListElement.append(node);
          filesListElement.append(document.createElement("br"));
        });
      }
    });
    var notes = document.getElementById("uploadedNotes");
    $.ajax({
      data: "id=" + id,
      url: "database_calls/ret_notes.php",
      async: false,
      method: "POST",
      success: function(msg) {
        if (!msg) return;
        var data = JSON.parse(msg);
        data.forEach(function(ele) {
          var node = document.createElement("li");
          node.innerText = ele["text"];
          node.classList.add("list-group-item");
          node.setAttribute("data-toggle", "tooltip");
          node.setAttribute("title", "Added by " + ele["doneby"]);
          var node2 = document.createElement("span");
          node2.classList.add("glyphicon", "glyphicon-remove");
          node2.addEventListener("click", function() {
            deleteNote(this, ele["note_id"]);
          });
          node2.style.cursor = "pointer";
          node2.style.position = "absolute";
          node2.style.right = "10px";
          node2.style.paddingTop = "10px";
          node.append(node2);
          notes.append(node);
        });
      }
    });
    if (window.deleteListAnon)
      document
        .getElementById("deleteListBtn")
        .removeEventListener("click", window.deleteListAnon);
    window.deleteListAnon = function() {
      deleteList(window.listID);
    };
    document
      .getElementById("deleteListBtn")
      .addEventListener("click", window.deleteListAnon);
  });

  document
    .getElementById("fileToUpload")
    .addEventListener("change", function() {
      $("#fileUploadForm").append(
        '<input type="hidden" name="list_id" value="' + window.listID + '" /> '
      );
      $("#fileUploadForm").off();
      $("#fileUploadForm").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
          type: "POST",
          url: "database_calls/upload_file.php",
          async: true,
          data: new FormData(this),
          dataType: "json",
          contentType: false,
          cache: false,
          processData: false,
          complete: function(response) {
            if (response.responseText.split(" ")[0] != "success")
              alert(response.responseText);
            else {
              var filesListElement = document.getElementById("uploadedFiles");
              var node = document.createElement("p");
              node.innerText = response.responseText
                .split(" ")[1]
                .split("/")[1];
              node.addEventListener("click", function() {
                window.open(
                  "database_calls/" + response.responseText.split(" ")[1]
                );
              });
              node.style.position = "absolute";
              node.style.left = "15px";
              node.style.float = "left";
              node.style.cursor = "pointer";
              node.setAttribute("data-toggle", "tooltip");
              node.setAttribute("title", "Added by you");
              filesListElement.append(node);
              node = document.createElement("span");
              node.classList.add("glyphicon", "glyphicon-remove");
              node.addEventListener("click", function() {
                deleteFile(this, response.responseText.split(" ")[1]);
              });
              node.style.cursor = "pointer";
              node.style.float = "right";
              filesListElement.append(node);
              filesListElement.append(document.createElement("br"));
            }
          }
        });
        document.getElementById("fileToUpload").value = "";
      });
      $("#fileUploadForm").submit();
    });
});
